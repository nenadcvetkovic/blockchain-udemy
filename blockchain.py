# Initializing our (empty) blockchain list
genesis_block = {
    'previous_hash': '',
    'index': 0,
    'transactions': []
}
blockchain = [genesis_block]
open_transaction = []
owner = 'Max'


def hash_block(block):
    return '-'.join([str(block[key]) for key in block])

def get_last_blockchain_value():
    """ Returns the last value of the current blockchain. """
    if len(blockchain) < 1:
        return None
    return blockchain[-1]

# This function accepts two arguments
# One required one (transaction_amount) and one optional one (last trasaction)
# The optional one is optional because it has a default value => [1]
def add_transaction(recipient, sender=owner, amount=1.0):
    """ Append a new value as welll ...

    Arguments
        :sender: The amount....
        :recipients: Description...
        :amount: Default 1.0...
    """
    transaction = {
        'sender':sender,
        'recipient':recipient,
        'amount':amount
    }
    open_transaction.append(transaction)


def mine_block():
    last_block = blockchain[-1]
    hashed_block = hash_block(last_block)
    print(hashed_block)
    block = {
        'previous_hash': hashed_block,
        'index': len(blockchain),
        'transactions': open_transaction
    }
    blockchain.append(block)


def get_transaction_value():
    tx_recipient = raw_input('Enter the recipient of the transaction: ')
    tx_amount = float(input('Your transaction amount please: ')) 
    return tx_recipient,tx_amount
    

def get_user_choice():
    """ Prompts the user foor its choice and return it."""
    user_input = raw_input('Your choice: ')
    return user_input


def verify_chain():
    """ Verify the current blockchain and retrun True if it's valid, False other"""
    for (index,block) in enumerate(blockchain):
        if index == 0:
            continue
        if block['previous_hash'] != hash_block(blockchain[index - 1]):
            return False
    return True


def print_blockchain_elements():
    # Output the blockchain list to the console
    for block in blockchain:
        print('Outout Block')
        print(block)
    else:
        print('-' * 20)


waiting_for_input = True

while waiting_for_input:
    print('Please choose')
    print('1: Add a new trasaction value')
    print('2: Mine block')
    print('3: Output the blockchain blocks')
    print('h: Manioulate chain')
    print('q: Quit')
    user_choice = get_user_choice()
    if user_choice == '1':
        tx_data = get_transaction_value()
        recipient, amount = tx_data
        add_transaction(recipient, amount=amount)
        print(open_transaction)
    elif user_choice == '2':
        mine_block()
    elif user_choice == '3':
        print_blockchain_elements()
    elif user_choice == 'h':
        if len(blockchain) >= 1:
            blockchain[0] = {
                'previous_hash': '',
                'index': 0,
                'transaction': [{
                    'sender': 'Chris',
                    'recipient': 'Max',
                    'amount': 100.0
                }]
            }
    elif user_choice == 'q':
        waiting_for_input = False
    else:
        print('Input was invalid, please pick a value from the list %s' %(user_choice))
    if not verify_chain():
        print_blockchain_elements()
        print('Invalid blockchain!')
        break
else:
    print('User left!')

print('Done')