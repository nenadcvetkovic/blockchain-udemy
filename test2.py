names = ['ivan', 'dragan', 'milojica', 'dagoljupce']

for name in names:
    if len(name) >= 5:
        for letter in name:
            if letter == 'n' or letter == 'N':
                print('Len of name %s is %d' %(name,len(name)))

empty_list = False

while empty_list:
    if len(names) <= 0:
        names.pop()
    else:
        empty_list = True